defmodule OzoneWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build and query models.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest

      alias Ozone.Repo
      import Ecto
      import Ecto.Changeset
      import Ecto.Query

      import OzoneWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint OzoneWeb.Endpoint

      @default_opts [
        store: :cookie,
        key: "foobar",
        encryption_salt: "encrypted cookie salt",
        signing_salt: "signing salt"
      ]
      @secret String.duplicate("abcdef0123456789", 8)
      @signing_opts Plug.Session.init(Keyword.put(@default_opts, :encrypt, false))

      def conn_with_fetched_session(the_conn) do
				the_conn.secret_key_base
					|> put_in(@secret)
					|> Plug.Session.call(@signing_opts)
					|> Plug.Conn.fetch_session
      end

			def guardian_login(%Ozone.User{} = user) do
				conn = build_conn()
					|> conn_with_fetched_session
					|> Guardian.Plug.sign_in(user, :token, perms: %{default: Guardian.Permissions.max})
					|> Guardian.Plug.VerifySession.call(Map.new([]))
			end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Ozone.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Ozone.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
