defmodule OzoneWeb.AuthControllerTest do
  use OzoneWeb.ConnCase

  alias Ozone.User
  alias Ozone.Repo

	def user_fixture(attrs \\ %{}) do
		changeset = User.changeset(%User{}, attrs)
		case Repo.insert(changeset) do
			{:ok, user } -> user
			{:error, _} -> nil
		end
	end

  defp create_user(_) do
    timestamp = DateTime.to_unix(DateTime.utc_now())
    user = user_fixture(%{email: "email-#{timestamp}@example.com", username: "user-#{timestamp}"})
    {:ok, user: user}
  end

  describe "login when logged in" do
    setup [:create_user]

    test "redirects to root path", %{user: user} do
      conn = guardian_login(user)
      conn = get(conn, auth_path(conn, :login, :identity))
      assert redirected_to(conn) == page_path(conn, :index)
      assert get_flash(conn, :info) =~ "Already signed in"
    end
  end

  describe "login without user" do
    test "renders login page", %{conn: conn} do
      conn = get(conn, auth_path(conn, :login, :identity))
      assert html_response(conn, 200) =~ "Login"
    end
  end

  describe "logout when logged in" do
    setup [:create_user]

    test "redirects to root path", %{user: user} do
      conn = guardian_login(user)
      conn = delete(conn, auth_path(conn, :logout))
      assert redirected_to(conn) == page_path(conn, :index)
      assert get_flash(conn, :info) =~ "Signed out"
    end
  end

  describe "logout without user" do
    test "redirect to root path", %{conn: conn} do
      conn = delete(conn, auth_path(conn, :logout))
      assert redirected_to(conn) == page_path(conn, :index)
      assert get_flash(conn, :info) =~ "Not logged in"
    end
  end
end
