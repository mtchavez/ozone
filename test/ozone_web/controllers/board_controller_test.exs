defmodule OzoneWeb.BoardControllerTest do
  use OzoneWeb.ConnCase

  alias Ozone.User
  alias Ozone.Boards
  alias Ozone.User
  alias Ozone.Repo

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

	def user_fixture(attrs \\ %{}) do
		changeset = User.changeset(%User{}, attrs)
		case Repo.insert(changeset) do
			{:ok, user } -> user
			{:error, _} -> nil
		end
	end

  def fixture(:board) do
    timestamp = DateTime.to_unix(DateTime.utc_now())
    user = user_fixture(%{email: "email-#{timestamp}@example.com", username: "user-#{timestamp}"})
    {:ok, board} = Boards.create_board(user, @create_attrs)
    {board, user}
  end

  setup do
    {board, user} = fixture(:board)
		conn = guardian_login(user)
    {:ok, %{board: board, user: user, conn: conn}}
  end

  describe "index" do
    test "lists all boards", %{conn: conn} do
      conn = get(conn, board_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Boards"
    end
  end

  describe "new board" do
    test "renders form", %{conn: conn} do
      conn = get(conn, board_path(conn, :new))
      assert html_response(conn, 200) =~ "New Board"
    end
  end

  describe "create board" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, board_path(conn, :create), board: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == board_path(conn, :show, id)

      conn = get conn, board_path(conn, :show, id)
      assert html_response(conn, 200) =~ @create_attrs[:name]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, board_path(conn, :create), board: @invalid_attrs
      assert html_response(conn, 200) =~ "New Board"
    end
  end

  describe "edit board" do
    test "renders form for editing chosen board", %{conn: conn, board: board} do
      conn = get conn, board_path(conn, :edit, board)
      assert html_response(conn, 200) =~ "Edit Board"
    end
  end

  describe "update board" do
    test "redirects when data is valid", %{conn: conn, board: board} do
      conn = put conn, board_path(conn, :update, board), board: @update_attrs
      assert redirected_to(conn) == board_path(conn, :show, board)

      conn = get conn, board_path(conn, :show, board)
      assert html_response(conn, 200) =~ @update_attrs[:name]
    end

    test "renders errors when data is invalid", %{conn: conn, board: board} do
      conn = put conn, board_path(conn, :update, board), board: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Board"
    end
  end

  describe "delete board" do
    test "deletes chosen board", %{conn: conn, board: board} do
      conn = delete conn, board_path(conn, :delete, board)
      assert redirected_to(conn) == board_path(conn, :index)
    end
  end
end
