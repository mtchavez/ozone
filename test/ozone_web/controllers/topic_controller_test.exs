defmodule OzoneWeb.TopicControllerTest do
  use OzoneWeb.ConnCase

  alias Ozone.Boards
  alias Ozone.User

  @create_attrs %{body: "some body"}
  @update_attrs %{body: "some updated body"}
  @invalid_attrs %{body: nil, votes: nil, user_id: nil, board_id: nil}
  @valid_board_attrs %{name: "some name"}

  def user_fixture(attrs \\ %{}) do
    changeset = User.changeset(%User{}, attrs)
    case Repo.insert(changeset) do
      {:ok, user } -> user
      {:error, _} -> nil
    end
  end

  def board_fixture(attrs \\ %{}) do
    board_attrs =
      attrs
      |> Enum.into(@valid_board_attrs)

    timestamp = DateTime.to_unix(DateTime.utc_now())
    user = user_fixture(%{email: "email-#{timestamp}@example.com", username: "user-#{timestamp}"})
    {:ok, board} = Boards.create_board(user, board_attrs)

    {board, user}
  end

  def fixture(:topic, attrs \\ %{}) do
    {board, user} = board_fixture()
    {:ok, topic} =
      attrs
      |> Enum.into(@create_attrs)
      |> Enum.into(%{board_id: board.id, user_id: user.id})
      |> Boards.create_topic()

    {topic, board, user}
  end

  describe "index" do
    setup [:create_topic]

    test "lists all topics", %{conn: conn, board: board} do
      conn = get conn, board_topic_path(conn, :index, board)
      assert redirected_to(conn) == board_path(conn, :show, board)
      conn = get conn, board_path(conn, :show, board)
      assert html_response(conn, 200) =~ "Topics"
    end
  end

  describe "create topic" do
    setup [:create_topic]

    test "redirects to show when data is valid", %{conn: conn, board: board} do
      conn = post conn, board_topic_path(conn, :create, board), topic: @create_attrs
      assert redirected_to(conn) == board_path(conn, :show, board)

      conn = get conn, board_path(conn, :show, board)
      assert html_response(conn, 200) =~ "Topic some body created successfully"
    end

    test "renders errors when data is invalid", %{conn: conn, board: board} do
      conn = post conn, board_topic_path(conn, :create, board), topic: @invalid_attrs
      assert redirected_to(conn) == board_path(conn, :show, board)

      conn = get conn, board_path(conn, :show, board)
      assert html_response(conn, 200) =~ "Failed to add"
    end
  end

  describe "update topic" do
    setup [:create_topic]

    test "redirects when data is valid", %{conn: conn, topic: topic, board: board} do
      conn = put conn, board_topic_path(conn, :update, board, topic), topic: @update_attrs
      assert redirected_to(conn) == board_path(conn, :show, board)

      conn = get conn, board_path(conn, :show, board)
      assert html_response(conn, 200) =~ "some updated body"
    end

    test "renders errors when data is invalid", %{conn: conn, topic: topic, board: board} do
      conn = put conn, board_topic_path(conn, :update, board, topic), topic: @invalid_attrs
      assert redirected_to(conn) == board_path(conn, :show, board)

      conn = get conn, board_path(conn, :show, board)
      assert html_response(conn, 200) =~ "Failed to update"
    end
  end

  describe "delete topic" do
    setup [:create_topic]

    test "deletes chosen topic", %{conn: conn, topic: topic, board: board} do
      conn = delete conn, board_topic_path(conn, :delete, board, topic)
      assert redirected_to(conn) == board_path(conn, :show, board)
      # assert_error_sent 404, fn ->
      #   get conn, board_path(conn, :show, board)
      # end
    end
  end

  defp create_topic(_) do
    {topic, board, user} = fixture(:topic)
    conn = guardian_login(user)
    {:ok, conn: conn, topic: topic, board: board}
  end
end
