defmodule OzoneWeb.BoardChannelTest do
  use OzoneWeb.ChannelCase

  alias Ozone.Repo
  alias OzoneWeb.BoardChannel
  alias Ozone.User
  alias Ozone.Boards
  alias Ozone.Boards.Topic

  setup do
    {board, user} = fixture(:board)
    {:ok, _, socket} =
      "current_user"
      |> socket(%{current_user: user})
      |> subscribe_and_join(BoardChannel, "board:#{board.id}")
    {:ok, socket: socket, board: board, user: user}
  end

  def user_fixture(attrs \\ %{}) do
    changeset = User.changeset(%User{}, attrs)
    case Repo.insert(changeset) do
      {:ok, user } -> user
      {:error, _} -> nil
    end
  end

  def fixture(:board) do
    timestamp = DateTime.to_unix(DateTime.utc_now())
    user = user_fixture(%{email: "email-#{timestamp}@example.com", username: "user-#{timestamp}"})
    {:ok, board} = Boards.create_board(user, %{name: "some name"})
    {board, user}
  end

  test "new_chat_message", %{socket: socket} do
    chat = %{"msg" => "Hey guise!"}
    broadcast_from!(socket, "new_chat_message", chat)
    assert_push("new_chat_message", chat)
  end

  describe "create topic when bad board" do
    test "returns error", %{socket: socket} do
      ref = push socket, "create_topic", %{"board_id" => "42", "topic" => ""}
      assert_reply ref, :error, %{msg: "Failed to add topic"}
    end
  end

  describe "create topic" do
    test "broadcasts new_topic message", %{socket: socket, board: board, user: user} do
      push socket, "create_topic", %{"board_id" => board.id, "topic" => %{"body" => "Topic body"}}
      :timer.sleep 250
      topic = Topic |> last |> Repo.one
      topic_push = %{
        id: topic.id,
        body: topic.body,
        user_id: board.user_id,
        username: user.username
      }
      assert_push "new_topic", topic_push, socket
    end
  end

  describe "delete topic when bad board" do
    test "returns error", %{socket: socket, board: board} do
      {:ok, topic} = Boards.create_topic(%{"user_id" => board.user_id, "board_id" => board.id, "body" => "Topic body"})
      ref = push socket, "delete_topic", %{"board_id" => "42", "topic_id" => topic.id}
      assert_reply ref, :error, %{msg: "Failed to remove topic"}
    end
  end

  describe "delete topic" do
    test "broadcasts removed_topic message", %{socket: socket, board: board} do
      {:ok, topic} = Boards.create_topic(%{"user_id" => board.user_id, "board_id" => board.id, "body" => "Topic body"})
      push socket, "delete_topic", %{"board_id" => board.id, "topic_id" => topic.id}
      :timer.sleep 250
      topic_push = %{
        id: topic.id,
        board_id: board.id
      }
      assert_push "removed_topic", topic_push, socket
    end
  end

  describe "vote on topic when bad board" do
    test "returns error", %{socket: socket} do
      ref = push socket, "topic_voted_on", %{"board_id" => "42", "topic_id" => "42"}
      assert_reply ref, :error, %{msg: "Failed to vote on topic"}
    end
  end

  describe "vote on topic when bad topic" do
    test "returns error", %{socket: socket, board: board} do
      ref = push socket, "topic_voted_on", %{"board_id" => board.id, "topic_id" => "42"}
      assert_reply ref, :error, %{msg: "Failed to vote on topic"}
    end
  end

  describe "vote on topic" do
    test "broadcasts topic_votes_changed message", %{socket: socket, board: board} do
      votes_before = 5
      {:ok, topic} = Boards.create_topic(%{
        "user_id" => board.user_id,
        "board_id" => board.id,
        "body" => "Topic body",
        "votes" => votes_before
      })
      push socket, "topic_voted_on", %{"board_id" => board.id, "topic_id" => topic.id}
      :timer.sleep(250)
      topic = Topic |> last |> Repo.one
      voted_on_push = %{
        id: topic.id,
        user_id: board.user_id,
        votes: votes_before + 1
      }
      assert_push "topic_votes_changed", voted_on_push, socket
    end
  end
end
