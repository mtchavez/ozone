defmodule Ozone.BoardsTest do
  use Ozone.DataCase

  alias Ozone.Boards
  alias Ozone.User
  alias Ozone.Repo

  @valid_board_attrs %{name: "some name"}

  def user_fixture(attrs \\ %{}) do
    changeset = User.changeset(%User{}, attrs)
    case Repo.insert(changeset) do
      {:ok, user } -> user
      {:error, _} -> nil
    end
  end

  def board_fixture(attrs \\ %{}) do
    board_attrs =
      attrs
      |> Enum.into(@valid_board_attrs)

    timestamp = DateTime.to_unix(DateTime.utc_now())
    user = user_fixture(%{email: "email-#{timestamp}@example.com", username: "user-#{timestamp}"})
    {:ok, board} = Boards.create_board(user, board_attrs)

    {board, user}
  end

  describe "boards" do
    alias Ozone.Boards.Board

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    test "list_boards/0 returns all boards" do
      {board, user} = board_fixture()
      assert Boards.list_boards(user) == [board]
    end

    test "get_board!/1 returns the board with given id" do
      {board, user} = board_fixture()
      assert Boards.get_board!(user, board.id) == board
    end

    test "create_board/1 with valid data creates a board" do
      assert {%Board{} = board, _} = board_fixture()
      assert board.name == "some name"
    end

    test "create_board/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_board(%User{}, @invalid_attrs)
    end

    test "update_board/2 with valid data updates the board" do
      {board, _} = board_fixture()
      assert {:ok, board} = Boards.update_board(board, @update_attrs)
      assert %Board{} = board
      assert board.name == "some updated name"
    end

    test "update_board/2 with invalid data returns error changeset" do
      {board, user} = board_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_board(board, @invalid_attrs)
      assert board == Boards.get_board!(user, board.id)
    end

    test "delete_board/1 deletes the board" do
      {board, user} = board_fixture()
      assert {:ok, %Board{}} = Boards.delete_board(board)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_board!(user, board.id) end
    end

    test "change_board/1 returns a board changeset" do
      {board, _} = board_fixture()
      assert %Ecto.Changeset{} = Boards.change_board(board)
    end
  end

  describe "topics" do
    alias Ozone.Boards.Topic

    @valid_attrs %{body: "some body"}
    @update_attrs %{body: "some updated body"}
    @invalid_attrs %{body: nil, votes: nil, user_id: nil, board_id: nil}

    def topic_fixture(attrs \\ %{}) do
      {board, user} = board_fixture()
      {:ok, topic} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Enum.into(%{board_id: board.id, user_id: user.id})
        |> Boards.create_topic()

      topic
    end

    test "list_topics/0 returns all topics" do
      topic = topic_fixture()
      assert Boards.list_topics() == [topic]
    end

    test "get_topic!/1 returns the topic with given id" do
      topic = topic_fixture()
      assert Boards.get_topic!(topic.id) == topic
    end

    test "create_topic/1 with valid data creates a topic" do
      assert {:ok, %Topic{} = topic} = Boards.create_topic(@valid_attrs)
      assert topic.body == "some body"
    end

    test "create_topic/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_topic(@invalid_attrs)
    end

    test "update_topic/2 with valid data updates the topic" do
      topic = topic_fixture()
      assert {:ok, topic} = Boards.update_topic(topic, @update_attrs)
      assert %Topic{} = topic
      assert topic.body == "some updated body"
    end

    test "update_topic/2 with invalid data returns error changeset" do
      topic = topic_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_topic(topic, @invalid_attrs)
      assert topic == Boards.get_topic!(topic.id)
    end

    test "delete_topic/1 deletes the topic" do
      topic = topic_fixture()
      assert {:ok, %Topic{}} = Boards.delete_topic(topic)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_topic!(topic.id) end
    end

    test "change_topic/1 returns a topic changeset" do
      topic = topic_fixture()
      assert %Ecto.Changeset{} = Boards.change_topic(topic)
    end
  end
end
