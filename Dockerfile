FROM elixir:1.5

RUN set -xe \
  && apt-get update \
  && apt-get install -y \
    build-essential \
    curl \
    inotify-tools \
    postgresql-client

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs

ENV APP_DIR /app
RUN mkdir -p $APP_DIR

COPY . $APP_DIR

WORKDIR $APP_DIR

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix compile
RUN cd deps/bcrypt_elixir && make clean && make

CMD ["iex"]
