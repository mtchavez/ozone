defmodule OzoneWeb.TopicController do
  use OzoneWeb, :controller

  alias Ozone.Boards
  # alias Ozone.Boards.Topic

  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  def index(conn, %{"board_id" => board_id}, current_user, _claims) do
    board = get_board(current_user, board_id)
    conn
    |> redirect(to: board_path(conn, :show, board))
  end

  def create(conn, %{"board_id" => board_id, "topic" => topic_params}, current_user, _claims) do
    board = get_board(current_user, board_id)
    case Boards.create_topic(Map.merge(topic_params, %{"user_id" => current_user.id, "board_id" => board.id})) do
      {:ok, topic} ->
        conn
        |> put_flash(:info, "Topic #{topic.body} created successfully.")
        |> redirect(to: board_path(conn, :show, board))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:info, "Failed to add")
        |> redirect(to: board_path(conn, :show, board), changeset: changeset)
    end
  end

  def update(conn, %{"board_id" => board_id, "id" => id, "topic" => topic_params}, current_user, _claims) do
    board = get_board(current_user, board_id)
    topic = Boards.get_topic!(id)

    case Boards.update_topic(topic, topic_params) do
      {:ok, topic} ->
        conn
        |> put_flash(:info, "Topic updated successfully.")
        |> redirect(to: board_path(conn, :show, board))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:info, "Failed to update")
        |> redirect(to: board_path(conn, :show, board), changeset: changeset)
    end
  end

  def delete(conn, %{"board_id" => board_id, "id" => id}, current_user, _claims) do
    board = get_board(current_user, board_id)
    topic = Boards.get_topic!(id)
    {:ok, _topic} = Boards.delete_topic(topic)

    conn
    |> put_flash(:info, "Topic deleted successfully.")
    |> redirect(to: board_path(conn, :show, board))
  end

  defp get_board(current_user, board_id) do
    Boards.get_board!(current_user, board_id)
  end
end
