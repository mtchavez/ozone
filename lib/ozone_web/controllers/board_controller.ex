defmodule OzoneWeb.BoardController do
  use OzoneWeb, :controller

  alias Ozone.Boards
  alias Ozone.Boards.Board
  alias Ozone.Boards.Topic

  plug(Guardian.Plug.EnsureAuthenticated, handler: __MODULE__)

  def index(conn, _params, current_user, _claims) do
    boards = Boards.list_boards(current_user)
    render(conn, "index.html", current_user: current_user, boards: boards)
  end

  def new(conn, _params, current_user, _claims) do
    changeset = Boards.change_board(%Board{})
    render(conn, "new.html", current_user: current_user, changeset: changeset)
  end

  def create(conn, %{"board" => board_params}, current_user, _claims) do
    case Boards.create_board(current_user, board_params) do
      {:ok, board} ->
        conn
        |> put_flash(:info, "Board created successfully.")
        |> redirect(to: board_path(conn, :show, board))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", current_user: current_user, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}, current_user, _claims) do
    board = Boards.get_board!(current_user, id)
    changeset = Ozone.Boards.change_topic(%Topic{}, %{board_id: board.id})
    topics = Boards.get_topics!(board)

    render(
      conn,
      "show.html",
      current_user: current_user,
      board: board,
      topics: topics,
      changeset: changeset
    )
  end

  def edit(conn, %{"id" => id}, current_user, _claims) do
    board = Boards.get_board!(current_user, id)
    changeset = Boards.change_board(board)
    render(conn, "edit.html", current_user: current_user, board: board, changeset: changeset)
  end

  def update(conn, %{"id" => id, "board" => board_params}, current_user, _claims) do
    board = Boards.get_board!(current_user, id)

    case Boards.update_board(board, board_params) do
      {:ok, board} ->
        conn
        |> put_flash(:info, "Board updated successfully.")
        |> redirect(to: board_path(conn, :show, board))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", current_user: current_user, board: board, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user, _claims) do
    board = Boards.get_board!(current_user, id)
    {:ok, _board} = Boards.delete_board(board)

    conn
    |> put_flash(:info, "Board deleted successfully.")
    |> redirect(to: board_path(conn, :index))
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:info, "Authentication required")
    |> redirect(to: "/")
  end
end
