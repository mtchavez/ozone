defmodule OzoneWeb.Router do
  use OzoneWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  # This plug will look for a Guardian token in the session in the default location
  # Then it will attempt to load the resource found in the JWT.
  # If it doesn't find a JWT in the default location it doesn't do anything
  pipeline :browser_auth do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
    plug :put_user_token
  end

  # We need this pipeline to load the token when we're impersonating.
  # We don't want to load the resource though, just verify the token
  pipeline :impersonation_browser_auth do
    plug Guardian.Plug.VerifySession, key: :admin
  end

  # This pipeline if intended for API requests and looks for the JWT in the "Authorization" header
  # In this case, it should be prefixed with "Bearer" so that it's looking for
  # Authorization: Bearer <jwt>
  pipeline :api_auth do
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", OzoneWeb do
    pipe_through [:browser, :browser_auth, :impersonation_browser_auth]

    resources "/users", UserController
    resources "/boards", BoardController do
      resources "/topics", TopicController, only: [:index, :create, :update, :delete]
    end
    get "/", PageController, :index
  end

  scope "/auth", OzoneWeb do
    pipe_through [:browser, :browser_auth]

    get "/:provider", AuthController, :login
    get "/:provider/callback", AuthController, :callback
    post "/:provider/callback", AuthController, :callback
    delete "/logout", AuthController, :logout
  end

  scope "/api", OzoneWeb do
    pipe_through [:api, :api_auth]
  end

  defp put_user_token(conn, _) do
    if current_user = Guardian.Plug.current_resource(conn, :default) do
      token = Phoenix.Token.sign(conn, "user_web_socket", current_user.id)
      assign(conn, :user_token, token)
    else
      conn
    end
  end
end
