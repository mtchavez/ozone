defmodule OzoneWeb.BoardChannel do
  use OzoneWeb, :channel
  alias Ozone.Repo
  alias Ozone.Boards
  alias Ozone.Boards.Topic

  def join("board:" <> board_id, _payload, socket) do
    if authorized?(board_id, socket) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_in("new_chat_message", %{"msg" => msg}, socket) do
    broadcast socket, "new_chat_message", %{
      user: current_user(socket).username,
      timestamp: System.system_time(:seconds),
      msg: msg
    }
    {:noreply, socket}
  end

  def handle_in("create_topic", %{"board_id" => board_id, "topic" => topic_params}, socket) do
    user = current_user(socket)
    topic_attrs = case get_board(user, board_id) do
      {:ok, board} ->
        Map.merge(topic_params, %{"user_id" => user.id, "board_id" => board.id})
      _ ->
       %{}
    end
    case Boards.create_topic(topic_attrs) do
      {:ok, topic} ->
        broadcast socket, "new_topic", %{
          id: topic.id,
          body: topic.body,
          user_id: topic.user_id,
          board_id: board_id,
          username: user.username
        }
        {:noreply, socket}
      {:error, %Ecto.Changeset{}} ->
        {:reply, {:error, %{msg: "Failed to add topic"}}, socket}
    end
  end

  def handle_in("delete_topic", %{"board_id" => board_id, "topic_id" => topic_id}, socket) do
    user = current_user(socket)
    case get_board(user, board_id) do
      {:ok, board} ->
        topic = Boards.get_board_topic(board.id, topic_id)
        case Boards.delete_topic(topic) do
          {:ok, topic} ->
            broadcast socket, "removed_topic", %{
              id: topic.id,
              board_id: board_id
            }
            {:noreply, socket}
          {:error, %Ecto.Changeset{}} ->
            {:reply, {:error, %{msg: "Failed to remove topic"}}, socket}
        end
      _ ->
        {:reply, {:error, %{msg: "Failed to remove topic"}}, socket}
    end
  end

  def handle_in("topic_voted_on", %{"board_id" => board_id, "topic_id" => topic_id}, socket) do
    user = current_user(socket)
    case get_board(user, board_id) do
      {:ok, board} ->
        topic = Boards.get_board_topic(board.id, topic_id)
        if topic == nil do
          {:reply, {:error, %{msg: "Failed to vote on topic"}}, socket}
        else
          topic_changeset = Topic.votes_changeset(topic, (topic.votes || 0) + 1)
          case Repo.update(topic_changeset) do
            {:ok, topic} ->
              broadcast socket, "topic_votes_changed", %{
                id: topic.id,
                board_id: board_id,
                votes: topic.votes
              }
              {:noreply, socket}
            _ ->
              {:reply, {:error, %{msg: "Failed to vote on topic"}}, socket}
          end
        end
      _ ->
        {:reply, {:error, %{msg: "Failed to vote on topic"}}, socket}
    end
  end

  # Add authorization logic here as required.
  defp authorized?(board_id, socket) do
    user = current_user(socket)
    try do
      board = Boards.get_board!(user, board_id)
      if board.id do
         true
      else
        false
      end
    rescue
      Ecto.NoResultsError -> false
    end
  end

  defp current_user(socket) do
    socket.assigns.current_user
  end

  defp get_board(user, board_id) do
    try do
      board = Boards.get_board!(user, board_id)
      {:ok, board}
    rescue
      _ -> {:error}
    end
  end
end
