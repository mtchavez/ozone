defmodule Ozone.Boards.Topic do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ozone.Boards.Topic

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "topics" do
    field :body, :string
    field :votes, :integer, default: 0
    belongs_to :user, Ozone.User
    belongs_to :board, Ozone.Boards.Board

    timestamps()
  end

  @doc false
  def changeset(%Topic{} = topic, attrs) do
    topic
    |> cast(attrs, [:body, :votes, :user_id, :board_id])
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:board_id)
    |> validate_required([:body])
  end

  def votes_changeset(model, votes) do
    model
    |> change(%{votes: votes})
  end
end
