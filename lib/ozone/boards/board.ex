defmodule Ozone.Boards.Board do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ozone.Boards.Board

  @primary_key {:id, :binary_id, autogenerate: true, read_after_write: true}
  @foreign_key_type :binary_id
  schema "boards" do
    field :name, :string

    belongs_to :user, Ozone.User
    has_many :topics, Ozone.Boards.Topic

    timestamps()
  end

  @doc false
  def changeset(%Board{} = board, attrs) do
    board
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
