defmodule Ozone.Authorization do
  use Ecto.Schema
  use OzoneWeb, :model

  @primary_key {:id, :binary_id, autogenerate: true, read_after_write: true}
  @foreign_key_type :binary_id
  schema "authorizations" do
    field :expires_at, :integer
    field :provider, :string
    field :refresh_token, :string
    field :token, :string
    field :uid, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    belongs_to :user, Ozone.User

    timestamps()
  end

  @required_fields ~w(provider uid user_id token)a
  @optional_fields ~w(refresh_token expires_at)a

  @doc """
  Creates a changeset based on the `model` and `params`.
  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:provider_uid)
  end
end
