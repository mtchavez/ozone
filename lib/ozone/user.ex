defmodule Ozone.User do
  use Ecto.Schema
  use OzoneWeb, :model

  @primary_key {:id, :binary_id, autogenerate: true, read_after_write: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, :string
    field :username, :string

    has_many :authorizations, Ozone.Authorization
    has_many :boards, Ozone.Boards.Board

    timestamps()
  end

  @required_fields ~w(email username)a
  @optional_fields ~w()a

  def registration_changeset(model, params \\ :empty) do
    model
    |> cast(params, ~w(email username)a, [])
    |> unique_constraint(:email, message: "Email already taken")
    |> unique_constraint(:username, message: "Username already taken")
    |> validate_required(@required_fields)
  end

  @doc """
  Creates a changeset based on the `model` and `params`.
  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields ++ @optional_fields, [])
    |> unique_constraint(:email, message: "Email already taken")
    |> unique_constraint(:username, message: "Username already taken")
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/@/)
  end
end
