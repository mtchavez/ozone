defmodule Ozone.Mixfile do
  use Mix.Project

  def project do
    [app: :ozone,
     version: "0.0.1",
     elixir: "~> 1.5",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     test_coverage: [tool: ExCoveralls],
     preferred_cli_env: [
       "coveralls": :test,
       "coveralls.detail": :test,
       "coveralls.post": :test,
       "coveralls.html": :test
     ],
     aliases: aliases(),
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Ozone.Application, []},
      extra_applications: applications(Mix.env)
    ]
  end

  def applications(env) when env in [:test] do
    applications(:default) ++ []
  end

  def applications(_) do
    [
      :comeonin,
      :cowboy,
      :ecto,
      :logger,
      :phoenix,
      :phoenix_ecto,
      :phoenix_html,
      :postgrex,
      :runtime_tools,
      :ueberauth,
      :ueberauth_gitlab_strategy,
      :ueberauth_identity,
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:bcrypt_elixir, "~> 0.12"},
      {:comeonin, "~> 4.1"},
      {:cowboy, "~> 1.0"},
      {:gettext, "~> 0.11"},
      {:guardian, "~> 0.14"},
      {:guardian_db, "~> 0.8"},
      {:phoenix, "~> 1.3.0"},
      {:phoenix_ecto, "~> 3.3"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_pubsub, "~> 1.0"},
      {:postgrex, ">= 0.0.0"},
      {:ueberauth, "~> 0.4"},
      {:ueberauth_gitlab_strategy, "~> 0.2"},
      {:ueberauth_identity, "~>0.2.3"},

      # Dev
      {:dogma, "~> 0.1", only: :dev},
      {:phoenix_live_reload, "~> 1.0", only: :dev},

      # Test
      {:excoveralls, "~> 0.7", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test": ["ecto.create --quiet", "ecto.migrate", "test"],
      "app.up": ["deps.compile", "ecto.setup", "phx.server"]
    ]
  end
end
