export default class Board {

  constructor(socket, elem) {
    if (!elem) { return }
    let boardId = elem.getAttribute("data-id")
    socket.connect()
    this.channel = socket.channel(`board:${boardId}`)
    this.onReady(boardId, socket)
  }

  onReady(boardId, socket) {
    this.listenForChat()
    this.listenForTopics(boardId)
    this.channel.join()
      .receive("ok", resp => console.log(`Joined the Board[${boardId}] channel`))
      .receive("error", reason => console.log(`Failed to join board channel: ${reason}`))
  }

  listenForTopics(boardId) {
    this.channel.on("new_topic", resp => {
      let topicsContainer = document.getElementById("board-topics-container")
      let msgElem = document.createElement("li")
      msgElem.className = "list-group-item topic"
      msgElem.setAttribute("data-id", resp.id)
      let removeLink = document.createElement("a")
      removeLink.className ="delete-topic pull-right"
      removeLink.setAttribute("onClick", `if(window.boardInstance) { window.boardInstance.deleteTopic("${resp.board_id}", "${resp.id}") }`)
      let icon = document.createElement("i")
      icon.className = "fas fa-trash"
      removeLink.appendChild(icon)
      msgElem.innerHTML = resp.body
      msgElem.appendChild(removeLink)
      topicsContainer.appendChild(msgElem)
    })
    this.setupTopicListener(boardId)
  }

  setupTopicListener(boardId) {
    let topicSubmitBtn = document.getElementById("topic-submit-btn")
    let topicBody = document.getElementById("topic_body")
    topicSubmitBtn.addEventListener("click", ev => {
      ev.preventDefault()
      ev.stopPropagation()
      let payload = { board_id: boardId, topic: { body: topicBody.value } }
      this.channel.push("create_topic", payload)
      topicBody.value = ""
    })
    this.listenForDeletedTopic()
    this.listenForVotes()
  }

  listenForVotes() {
    this.channel.on("topic_votes_changed", resp => {
      let topicVotes = document.querySelector(`li.topic[data-id='${resp.id}'] span.votes`)
      topicVotes.setAttribute('data-votes', resp.votes)
      topicVotes.innerHTML = `(${resp.votes})`
    })
  }

  voteOnTopic(boardId, topicId) {
    this.channel.push("topic_voted_on", {board_id: boardId, topic_id: topicId})
  }

  listenForDeletedTopic() {
    this.channel.on("removed_topic", resp => {
      let chatContainer = document.getElementById("board-chat-container")
      let topic = document.querySelector(`li.topic[data-id='${resp.id}']`)
      if (topic) {
        topic.remove()
      }
    })
  }

  deleteTopic(boardId, topicId) {
    this.channel.push("delete_topic", {board_id: boardId, topic_id: topicId})
  }

  listenForChat() {
    this.channel.on("new_chat_message", resp => {
      let chatContainer = document.getElementById("board-chat-container")
      let msgElem = document.createElement("li")
      msgElem.className = "list-group-item"
      msgElem.innerHTML = resp.msg
      chatContainer.appendChild(msgElem)
    })
    this.setupChatListener()
  }

  setupChatListener() {
    let chatSendBtn = document.getElementById("board-chat-msg-send")
    let chatMsg = document.getElementById("board-chat-msg-input")
    chatSendBtn.addEventListener("click", ev => {
      let payload = { msg: chatMsg.value }
      this.channel.push("new_chat_message", payload)
      chatMsg.value = ""
    })
  }
}
