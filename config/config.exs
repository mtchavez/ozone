# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ozone,
  ecto_repos: [Ozone.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :ozone, OzoneWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "i1XcI5MXMICoWo9UC71P64oMIzIUjnAOVMsl9nYZkoDoWWltLBEGMPRq1pgoQqH0",
  render_errors: [view: Ozone.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ozone.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :guardian, Guardian,
  issuer: "Ozone.#{Mix.env}",
  ttl: {30, :days},
  verify_issuer: true,
  serializer: OzoneWeb.GuardianSerializer,
  secret_key: to_string(Mix.env),
  hooks: GuardianDb,
  permissions: %{
    default: [
      :read_profile,
      :write_profile,
      :read_token,
      :revoke_token,
    ],
  }

config :guardian_db, GuardianDb,
  repo: Ozone.Repo,
  sweep_interval: 60 # 60 minutes

config :ueberauth, Ueberauth.Strategy.Gitlab.OAuth,
  client_id: System.get_env("GITLAB_CLIENT_ID"),
  client_secret: System.get_env("GITLAB_CLIENT_SECRET"),
  redirect_uri: System.get_env("GITLAB_REDIRECT_URI")

config :ueberauth, Ueberauth,
  providers: [
    identity: { Ueberauth.Strategy.Identity, [
        callback_methods: ["POST"],
        uid_field: :email,
        nickname_field: :username,
      ] },
    gitlab: {Ueberauth.Strategy.Gitlab, [default_scope: "read_user"]},
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
