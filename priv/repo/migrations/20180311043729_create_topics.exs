defmodule Ozone.Repo.Migrations.CreateTopics do
  use Ecto.Migration

  def change do
    create table(:topics, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :body, :text
      add :votes, :integer
      add :board_id, references(:boards, on_delete: :nothing, type: :binary_id)
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:topics, [:board_id])
    create index(:topics, [:user_id])
  end
end
