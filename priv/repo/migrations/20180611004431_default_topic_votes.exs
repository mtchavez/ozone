defmodule Ozone.Repo.Migrations.DefaultTopicVotes do
  use Ecto.Migration

  def change do
    alter table("topics") do
      modify :votes, :integer, default: 0
    end
  end
end
