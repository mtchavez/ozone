defmodule Ozone.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    execute("CREATE EXTENSION IF NOT EXISTS citext;")
    execute(~s(CREATE EXTENSION IF NOT EXISTS "uuid-ossp";))

    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :username, :citext
      add :email, :citext

      timestamps()
    end

    create index(:users, [:email], unique: true)
  end
end
